import * as functions from 'firebase-functions'
import * as admin from 'firebase-admin'
import * as Stripe from 'stripe'

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//


const generate_payment_response = (intent) => {
  if (intent.status === 'requires_action' && intent.next_action.type === 'use_stripe_sdk') {
    console.log('requires_action')
    return {
      requires_action: true,
      payment_intent_client_secret: intent.client_secret
    }
  } else if (intent.status === 'succeeded') {
    console.log('succeeded')
    return {
      success: true
    }
  } else {
    console.error('whoopsie')
    return {
      error: 'Invalid PI status',
      status: intent.status
    }
  }
}

// const capture_payment = async (amount) => {

// }

export const add_realtime_order = functions.https.onRequest(async (req, res) => {
  admin.initializeApp(functions.config().firebase)
  const db = admin.firestore()
  const uid = req.body.uid
  const order = {
    cart: req.body.order,
    uid: uid,
    isActive: true
  }
  // db.collection("users").doc(uid).collection("orders").doc('45645604').set(order).then((v) => {
  //   res.send({message: 'Order added'}).status(200).end()
  // }).catch((err) => {
  //   res.send({error: err}).status(500).end()
  // })
  // db.collection("orders").doc(uid).collection("orders").doc('4512123132').set(order).then((v) => {
  //   res.send({message: 'Order added'}).status(200).end()
  // }).catch((err) => {
  //   res.send({error: err}).status(500).end()
  // })
  db.collection('orders').doc('465345064052').set(order).then((v) => {
    res.send({message: 'success'}).status(200).end()
  }).catch((err) => {
    res.send({message: err}).status(500).end()
  })
})

export const create_customer = functions.https.onRequest(async (req, res) => {
  const stripe = new Stripe('sk_test_7tgUm3j4gMm9glBUGdXY3r6c')
  await stripe.customers.create(req.body, 
    async (err, customer) => {
      if (err) {
        res.send({error: err}).status(500).end()
      } else {
        res.send({message: 'success', customer: customer}).status(200).end()
      }
    })
})

export const fetch_customer = functions.https.onRequest(async (req, res) => {
  const stripe = new Stripe('sk_test_7tgUm3j4gMm9glBUGdXY3r6c')
  await stripe.customers.retrieve(req.body, async (err, customer) => {
    if (err) {
      res.send({error: err}).status(500).end()
    } else {
      res.send(customer).status(200).end()
    }
  })
})

export const get_secret_key = functions.https.onRequest((req, res) => {
  res.send('pk_test_TYooMQauvdEDq54NiTphI7jx').status(200).end()
})

export const helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!")
})

export const create_payment_intent = functions.https.onRequest(async (req, res) => {
  const stripe = new Stripe('sk_test_7tgUm3j4gMm9glBUGdXY3r6c')
  console.log('executing')
  const price = req.body.price
  await stripe.paymentIntents.create({
    amount: price,
    currency: 'gbp',
    payment_method_types: ['card']
  }, async (err: any, intent: any) => {
    if (err) {
      console.error(err)
      res.send({error: err}).status(500).end()
    }
    console.log(intent)
    res.send(intent).status(200).end()
  })
})

export const update_payment_intent = functions.https.onRequest(async (req, res) => {
  const stripe = new Stripe('sk_test_7tgUm3j4gMm9glBUGdXY3r6c')
  await stripe.paymentIntents.update(req.body.payment_intent_id, { 
    amount: req.body.amount
  }, async () => {
    res.send({stripe_msg: 'payment intent updated'}).status(200).end()
  })
})

export const create_checkout_session = functions.https.onRequest(async (req, res) => {
  const cart = req.body.cart
  console.log(cart)
  const stripe = new Stripe('sk_test_7tgUm3j4gMm9glBUGdXY3r6c')
  await stripe.checkout.sessions.create({
    cancel_url: 'https://ourplacecoffee.co.uk/#/payment_failed',
    payment_method_types: ['card'],
    success_url: 'https://ourplacecoffee.co.uk/#/payment_success',
    line_items: req.body.cart,
    customer: req.body.customer
  }, async (err, session) => {
    if (err) {
      console.log(err.message)
    } else {
      res.send({checkout_session_id: session.id}).status(200).end(() => {console.log('sent checkout ID')})
    }
  })
})

export const confirm_payment = functions.https.onRequest(async (req, res) => {
  const stripe = new Stripe('sk_test_7tgUm3j4gMm9glBUGdXY3r6c')
  console.log('hello')
  try {
    let intent
    console.log(req.body)
    if (req.body.payment_method_id) {
      console.log('has payment method id')
      // missing params must be added to index.d.ts in @types/stripe in node_modules
      intent = await stripe.paymentIntents.create({
        payment_method_types: ['card'],
        amount: req.body.price,
        currency: 'gbp',
        confirmation_method: 'manual',
        confirm: true,
        payment_method: req.body.payment_method_id,
      })
      res.send(generate_payment_response(intent)).status(200)
    } else if (req.body.payment_intent_id) {
      console.log('has payment intent id')
      intent = await stripe.paymentIntents.confirm(req.body.payment_intent_id)
      res.send(generate_payment_response(intent)).status(200)
    } else {
      console.error('whet')
      res.send({error: 'oopsie'}).status(500).end()
    }
  } catch(e) {
    res.send({error: e.message}).status(500)
  }
})

export const create_ephemeral_key = functions.https.onRequest(async (req, res) => {
  const stripe_version = req.query.api_version
  if (!stripe_version) {
    console.error('stripe version not found')
    res.status(400).end()
    return
  }
  const stripe = new Stripe('sk_test_7tgUm3j4gMm9glBUGdXY3r6c')
  stripe.ephemeralKeys.create(
    {customer: req.body.customerId},
    {stripe_version: stripe_version})
    .then((key) => {
      console.log('key')
      res.status(200).end()
    })
    .catch((err) => {
      console.error(err)
      res.status(500).end()
    })
})