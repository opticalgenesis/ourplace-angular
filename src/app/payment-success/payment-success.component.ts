import { Component, OnInit } from '@angular/core';
import { HotDrink } from '../hot-drink/hot-drink.component';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.component.html',
  styleUrls: ['./payment-success.component.scss']
})
export class PaymentSuccessComponent implements OnInit {

  cart: HotDrink[]
  afAuth: AngularFireAuth

  constructor(afAuth: AngularFireAuth) {
    this.cart = JSON.parse(sessionStorage.getItem('striped_cart')) as HotDrink[]
    this.afAuth = afAuth
    let addition = {
      order: this.cart,
    }
    this.addToUser(this.cart)
   }

  addToUser(cart) {
    this.afAuth.authState.subscribe(async (v) => {
      const json = await fetch('/add_realtime_order', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({
          order: cart,
          uid: v.uid
        })
      })
      let j = await json
      sessionStorage.setItem('striped_cart', null)
      console.log(j)
    })
  }

  ngOnInit() {
  }

}
