import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore'; 
import { environment } from '../environments/environment';
import { RouterModule, Routes } from '@angular/router';

import { MatButtonModule, MatCheckboxModule, MatToolbarModule, MatIconModule, MatSidenavModule, MatButtonToggleModule, MatListModule, MatGridListModule, MatDialogModule, MatFormFieldModule, MatSelectModule, MatInputModule, MAT_LABEL_GLOBAL_OPTIONS } from '@angular/material';

import { AppComponent } from './app.component';
import { HotDrinkComponent } from './hot-drink/hot-drink.component';
import { LandingComponentComponent } from './landing-component/landing-component.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HotDrinkCustomiserDialogComponent } from './hot-drink-customiser-dialog/hot-drink-customiser-dialog.component';
import { PaymentComponent } from './payment/payment.component';
import { CartComponent } from './cart/cart.component';
import { StripeDialogComponent } from './stripe-dialog/stripe-dialog.component';
import { PaymentFailedComponent } from './payment-failed/payment-failed.component';
import { PaymentSuccessComponent } from './payment-success/payment-success.component';
import { NgxAuthFirebaseUIModule } from 'ngx-auth-firebaseui';
import { AuthDialogComponent } from './auth-dialog/auth-dialog.component';

const appRoutes: Routes = [
  { path: '', component: LandingComponentComponent, data: {title: 'landing'} },
  { path: 'home', component: LandingComponentComponent, data: {title: 'landing'} },
  { path: 'hot_drink', component: HotDrinkComponent, data: {title: 'Hot Drink Menu'} },
  { path: 'payment', component: PaymentComponent, data: {title: 'Stripe'} },
  { path: 'cart', component: CartComponent, data: {title: 'Cart'} },
  { path: 'payment_failed', component: PaymentFailedComponent, data: {title: 'Payment Failed'} },
  { path: 'payment_success', component: PaymentSuccessComponent, data: {title: 'Payment Successful'} }
]

export function nameFactory(): string {
  return 'ourplace'
}

@NgModule({
  declarations: [
    AppComponent,
    HotDrinkComponent,
    LandingComponentComponent,
    NavigationComponent,
    HotDrinkCustomiserDialogComponent,
    PaymentComponent,
    CartComponent,
    StripeDialogComponent,
    PaymentFailedComponent,
    PaymentSuccessComponent,
    AuthDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatButtonToggleModule,
    MatListModule,
    MatGridListModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    RouterModule.forRoot(appRoutes, {useHash: true}),
    NgxAuthFirebaseUIModule.forRoot(environment.firebase, nameFactory, {
      enableFirestoreSync: true,
      toastMessageOnAuthSuccess: true,
      toastMessageOnAuthError: true
    })
  ],
  entryComponents: [
    HotDrinkCustomiserDialogComponent,
    StripeDialogComponent,
    AuthDialogComponent
  ],
  providers: [
    { provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'auto'} }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
