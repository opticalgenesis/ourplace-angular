import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotDrinkCustomiserDialogComponent } from './hot-drink-customiser-dialog.component';

describe('HotDrinkCustomiserDialogComponent', () => {
  let component: HotDrinkCustomiserDialogComponent;
  let fixture: ComponentFixture<HotDrinkCustomiserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotDrinkCustomiserDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotDrinkCustomiserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
