import { Component, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatSelect } from '@angular/material';
import { HotDrink } from '../hot-drink/hot-drink.component';

@Component({
  selector: 'app-hot-drink-customiser-dialog',
  templateUrl: './hot-drink-customiser-dialog.component.html',
  styleUrls: ['./hot-drink-customiser-dialog.component.scss']
})

export class HotDrinkCustomiserDialogComponent {

  blend: string

  constructor(public dialogRef: MatDialogRef<HotDrinkCustomiserDialogComponent>,
     @Inject(MAT_DIALOG_DATA) public data: HotDrink) { }

  onBlendSelected(blend: string) {
    this.blend = blend
  }

  addToCart() {
    this.dialogRef.close({blend: this.blend, shouldAdd: false})
  }
}
