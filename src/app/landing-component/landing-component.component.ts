import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-component',
  templateUrl: './landing-component.component.html',
  styleUrls: ['./landing-component.component.scss']
})
export class LandingComponentComponent implements OnInit {

  constructor() { 
    console.log('Landing')
  }

  ngOnInit() {
    console.log('ngOnInit - Landing')
  }

}
