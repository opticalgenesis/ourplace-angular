import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotDrinkComponent } from './hot-drink.component';

describe('HotDrinkComponent', () => {
  let component: HotDrinkComponent;
  let fixture: ComponentFixture<HotDrinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotDrinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotDrinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
